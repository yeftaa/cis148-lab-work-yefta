/**
 * Programmer: Meg Prescott
 * Date: 2017-09-21
 * CIS148 Week 4 Lab
 *
 * The purpose of this class is to perform a dice statistics experiment
 */
import java.util.Scanner;
public class DiceStats {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Dice dice = new Dice();

        int numRolls = 0;   // User defined number of rolls
        int numSixes = 0;   // Tracks number of 6s found
        int numSevens = 0;  // Tracks number of 7s found
        int total = 0;  // Sum of dice values

        System.out.println("Enter number of rolls: ");
        numRolls = input.nextInt();

        //Perform the dice roll experiment and display the outcome
        for (int i=1; i<=numRolls; i++) {
            dice.roll();
            total = dice.getTotal();
            /*
            if (total == 6) {
                numSixes++;
            } else if (total == 7) {
                numSevens++;
            }
            */
            switch(total) {
                case 6: numSixes++; break;
                case 7: numSevens++; break;
                default: break;
            }

            System.out.println("Roll " + i + " is " + total + " ("
                    + dice.getD1() + ", " + dice.getD2() + ")");
        }

        System.out.println("\nDice roll statistics:");
        System.out.println("6s: " + numSixes + " (" + (double)numSixes/numRolls*100 + "%)");
        System.out.println("7s: " + numSevens + " (" + (numSevens*1.0)/numRolls*100 + "%)");

        return;
    }
}

